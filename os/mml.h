#ifndef __MML_H_INCLUDED_
#define __MML_H_INCLUDED_

#define OCTAVE_NUM 7
#define NOTE_MAX 100 

int setTempoinSec(int tempo);
void printNote(int num, int note[NOTE_MAX][2]);
void setNote(int note[][2], int num, char sound, int octave, char half);
void startMML();


static int C[OCTAVE_NUM + 1] = {
	16, //C0
	33,
	65,
	131,
	262,
	523,
	1047,
	2093 //C7
};
static int D[OCTAVE_NUM + 1] = {
	19, //D0
	37,
	73,
	147,
	294,
	587,
	1175,
	2349 //D7
};
static int E[OCTAVE_NUM + 1] = {
	21, //E0
	41,
	82,
	165,
	370,
	659,
	1319,
	2637 //E7
};
static int F[OCTAVE_NUM + 1] = {
	22, //F0
	44,
	87,
	175,
	349,
	698,
	1397,
	2794 //F7
};
static int G[OCTAVE_NUM + 1] = {
	25, //G0
	49,
	98,
	196,
	392,
	784,
	1568,
	3136 //G7
};
static int A[OCTAVE_NUM + 1] = {
	28, //A0
	55,
	110,
	220,
	440,
	880,
	1760,
	3520 //A7
};
static int B[OCTAVE_NUM + 1] = {
	31, //B0
	62,
	123,
	247,
	494,
	988,
	1976,
	3951 //B7
};
static int CsDf[OCTAVE_NUM + 1] = {
	17,
	35,
	69,
	139,
	277,
	554,
	1109,
	2217
};
static int DsEf[OCTAVE_NUM + 1] = {
	19,
	39,
	78,
	156,
	311,
	622,
	1245,
	2489,
};
static int FsGf[OCTAVE_NUM + 1] = {
	23,
	46,
	92,
	185,
	370,
	740,
	1480,
	2960
};
static int GsAf[OCTAVE_NUM + 1] = {
	26,
	52,
	104,
	208,
	415,
	831,
	1661,
	3322
};
static int AsBf[OCTAVE_NUM + 1] = {
	29,
	58,
	117,
	233,
	466,
	932,
	1865,
	3729
};

#endif

