#ifndef _YMZ294_H_INCLUDED_
#define _YMZ294_H_INCLUDED_

#include "lib.h"
#include "../include/io.h"

#define WRCS_PIN P4DR.BIT.B0
#define A0_PIN   P4DR.BIT.B1
#define RESET_PIN P4DR.BIT.B2
#define DATA_PIN PADR.BYTE

#define YMZDRV_CMD_RESET 0
#define YMZDRV_CMD_PLAY  1

struct ymzreq {
    int freq;
    int msec;
};

void configureYMZ294();
void setRegister(char addr, char value);
void resetYMZ294();
void setFrequency(int ch, unsigned int freq);

#endif

