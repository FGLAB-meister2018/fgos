#include "ymz294.h"
#include "defines.h"
#include "kozos.h"
#include "lib.h"
#include "timerdrv.h"

static void send_start(int msec)
{
    struct timerreq *req;
    req = kz_kmalloc(sizeof(*req));
    req->id = MSGBOX_ID_YMZ294_WAIT;
    req->msec = msec;
    kz_send(MSGBOX_ID_TIMDRIVE, TIMERDRV_CMD_START, (char *)req);
}

void delay_ms(int msec)
{
    send_start(msec);
    kz_recv(MSGBOX_ID_YMZ294_WAIT, NULL, NULL);
    return;
}

void configureYMZ294()
{
    //set PA and P4 for output
    PADDR = 0xff;
    P4DDR = 0xff;

    resetYMZ294();

    setRegister(0x07, 0x38);
    setRegister(0x08, 0);
    setRegister(0x09, 0);
    setRegister(0x0a, 0);
    setFrequency(0, 0);
    setFrequency(1, 0);
    setFrequency(2, 0);

    delay_ms(100);

    return;
}

void setRegister(char addr, char value)
{
    WRCS_PIN = 0;
    A0_PIN   = 0;
    DATA_PIN = addr;
    WRCS_PIN = 1;

    WRCS_PIN = 0;
    A0_PIN   = 1;
    DATA_PIN = value;
    WRCS_PIN = 1;

    return;
}

void resetYMZ294()
{
    int i;

    for(i = 0x0; i < 0x0d; ++i)
        setRegister(i, 0);

    WRCS_PIN  = 1;
    A0_PIN    = 0;
    RESET_PIN = 0;
    delay_ms(10);
    RESET_PIN = 1;

    return;
}

void setFrequency(int ch, unsigned int freq)
{
    unsigned int cal_frequency = 0;

    if(freq != 0)
        cal_frequency = (int)125000 / freq;

    cal_frequency &= 0xfff;

    setRegister(0x00 + (ch * 2), cal_frequency & 0xff);
    setRegister(0x01 + (ch * 2), (cal_frequency >> 8) & 0xff);

    return;
}

static int ymzdrv_command(int cmd, char *p)
{
    struct ymzreq *req;
    req = (struct ymzreq *) p;

    switch(cmd) {
        case YMZDRV_CMD_RESET:
            configureYMZ294();
            break;

        case YMZDRV_CMD_PLAY:
            setFrequency(0, req->freq);
            setRegister(0x08, 15);
            delay_ms(req->msec);
            setRegister(0x08, 0);
            setFrequency(0, 0);
            kz_kmfree(p);
            break;

        default:
            break;
    }
    return 0;
}

int ymzdrv_main(int argc, char *argv[])
{
    int cmd;
    char *p; 

    configureYMZ294();

    while(1) {
        kz_recv(MSGBOX_ID_YMZ294, &cmd, &p);
        ymzdrv_command(cmd, p);
    } 

    return 0;
}
