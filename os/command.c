#include "defines.h"
#include "kozos.h"
#include "consdrv.h"
#include "timerdrv.h"
#include "lib.h"
#include "timer.h"
#include "intr.h"
#include "lib.h"
#include "../include/io.h"
#include "ymz294.h"
#include "mml.h"

static void send_use(int index)
{
    char *p;
    p = kz_kmalloc(3);
    p[0] = '0';
    p[1] = CONSDRV_CMD_USE;
    p[2] = '0' + index;
    kz_send(MSGBOX_ID_CONSOUTPUT, 3, p);
}

static void send_write(char *str)
{
    char *p;
    int len;
    len = strlen(str);
    p = kz_kmalloc(len + 2);
    p[0] = '0';
    p[1] = CONSDRV_CMD_WRITE;
    memcpy(&p[2], str, len);
    kz_send(MSGBOX_ID_CONSOUTPUT, len + 2, p);
}

static void send_start(int msec)
{
    struct timerreq *req;
    req = kz_kmalloc(sizeof(*req));
    req->id = MSGBOX_ID_CONSINPUT;
    req->msec = msec;
    kz_send(MSGBOX_ID_TIMDRIVE, TIMERDRV_CMD_START, (char *)req);
}

static void send_start_music()
{
    char *p;
    kz_send(MSGBOX_ID_MMLDRIVE, 1, p);
}

static void send_ymz_play(int freq, int msec)
{
    struct ymzreq *req;
    req = kz_kmalloc(sizeof(*req));
    req->freq = freq;
    req->msec = msec;
    kz_send(MSGBOX_ID_YMZ294, YMZDRV_CMD_PLAY, (char *)req);
}

static void send_ymz_reset()
{
    kz_send(MSGBOX_ID_YMZ294, YMZDRV_CMD_RESET, NULL);
}

int command_main(int argc, char *argv[])
{
    char *p;
    int size;

    send_use(SERIAL_DEFAULT_DEVICE);

    while(1) {
        send_write("command> ");

        kz_recv(MSGBOX_ID_CONSINPUT, &size, &p);
        if(p == NULL) {
            send_write("expired.\n");
            continue;
        }
        p[size] = '\0';

        if(!strncmp(p, "echo", 4)) {
            send_write(p + 4);
            send_write("\n");
        }
//        else if(!strncmp(p, "music", 5)) {
//            send_start_music();
//            startMML();
//        }
        else if(!strncmp(p, "play", 4)) {
            send_ymz_play(atoi(p + 5), 1000);
        }
        else if(!strncmp(p, "stop", 4)) {
            send_ymz_reset();
        }
        else if(!strncmp(p, "timer", 5)) {
            send_write("timer start.\n");
            send_start(2000);
        }
		else if(!strncmp(p, "help", 4)) {
            send_write("echo: display as echo\n");
            send_write("play: play a sound!\n");
            send_write("stop: stop a sound.\n");
            send_write("timer: set a timer.\n");
		}
        else {
            send_write("unknown\n");
        }

        kz_kmfree(p);
    }
    
    return 0;
}
