#include "mml.h"
#include "ymz294.h"
#include "consdrv.h"
#include "defines.h"
#include "kozos.h"
#include "lib.h"
#include "timerdrv.h"

char mml[] = "A l4o5 fga<c>g2 <fed>a<c2 defc&c>f. b-afc&c2 defb- ab<c>agab-g db-acge f.agf2";

static void send_write(char *str)    
{    
    char *p;    
    int len;    
    len = strlen(str);    
    p = kz_kmalloc(len + 2);    
    p[0] = '0';    
    p[1] = CONSDRV_CMD_WRITE;    
    memcpy(&p[2], str, len);    
    kz_send(MSGBOX_ID_CONSOUTPUT, len + 2, p);    
}  

static void send_start(int msec)
{
    struct timerreq *req;
    req = kz_kmalloc(sizeof(*req));
    req->id = MSGBOX_ID_MUSIC;
    req->msec = msec;
    kz_send(MSGBOX_ID_TIMDRIVE, TIMERDRV_CMD_START, (char *)req);
}

void mydelay_ms(int msec)
{
    send_start(msec);
    kz_recv(MSGBOX_ID_MUSIC, NULL, NULL);
    return;
}

int setTempoinSec(int tempo){
	int sec = 0;
	sec = (60*1000)/tempo;
	return sec;
}

void printNote(int num, int note[NOTE_MAX][2]){
	int i;
	for(i=0;i<num;i++){
	}
}

void setNote(int note[][2], int num, char sound, int octave, char half){
	//half=1 when sharp, =2 when flat, other when nothing
	switch(sound){
		case 'a':
			if(half==1)
				note[num][1] = AsBf[octave];
			else if(half==2)
				note[num][1] = GsAf[octave];
			else
				note[num][1] = A[octave];
			break;
		case 'b':
			if(half==2)
				note[num][1] = AsBf[octave];
			else
				note[num][1] = B[octave];
			break;
		case 'c':
			if(half==1)
				note[num][1] = CsDf[octave];
			else
				note[num][1] = C[octave];
			break;
		case 'd':
			if(half==1)
				note[num][1] = DsEf[octave];
			else if(half==2)
				note[num][1] = CsDf[octave];
			else
				note[num][1] = D[octave];
			break;
		case 'e':
			if(half==2)
				note[num][1] = DsEf[octave];
			else
				note[num][1] = E[octave];
			break;
		case 'f':
			if(half==1)
				note[num][1] = FsGf[octave];
			else
				note[num][1] = F[octave];
			break;
		case 'g':
			if(half==1)
				note[num][1] = GsAf[octave];
			else if(half==2)
				note[num][1] = FsGf[octave];
			else
				note[num][1] = G[octave];
			break;
		default:
			break;
	}
}

void startMML(){
	int i, j, n = 0, octave = 5, tempo = 100, sec, length = 4, note[NOTE_MAX][2];
	char tmp[3] = {};
	sec = setTempoinSec(tempo); //millisec par 1/4 note
    configureYMZ294();
	for(i=0;i<sizeof(mml);i++){
		if(!(mml[i] >= 'A' && mml[i] <= 'Z') && mml[i] != ' '){
			if(mml[i] == 'l'){
				length = mml[i+1]-'0';
				i++;
			} else if(mml[i] == 'o'){
				octave = mml[i+1]-'0';
				i++;
			} else if(mml[i] == 't'){
				for(j=1;mml[i+j]>='0'&&mml[i+j]<='9';j++){
					tmp[j-1] = mml[i+j];
				}
				tempo = atoi(tmp);
				sec = setTempoinSec(tempo); //millisec par 1/4 note
				i = i+j;
			} else if(mml[i] >= 'a' && mml[i] <= 'g'){
				if(mml[i+1] == '+'){
					setNote(note, n, mml[i], octave, 1);
					if((4/length)*sec>=1000){
                        note[n][2] = 999;
					} else {
					    note[n][2] = (4/length)*sec;
					}
					i++;
				} else if (mml[i+1] == '-'){
					setNote(note, n, mml[i], octave, 2);
					if((4/length)*sec>=1000){
                        note[n][2] = 999;
					} else {
					    note[n][2] = (4/length)*sec;
					}
					i++;
				} else if (mml[i+1] >= '0' && mml[i+1] <= '9'){
					setNote(note, n, mml[i], octave, 0);
					note[n][2] = (4/(mml[i+1]-'0'))*sec;
					if((4/(mml[i+1]-'0'))*sec>=1000){
                        note[n][2] = 999;
					} else {
					    note[n][2] = (4/(mml[i+1]-'0'))*sec;
					}
					i++;
				} else if (mml[i+1] == '&'){
					setNote(note, n, mml[i], octave, 0);
					if((4/length)*sec>=1000){
                        note[n][2] = 999;
					} else {
					    note[n][2] = (4/length)*sec;
					}
					i++;
				} else {
					setNote(note, n, mml[i], octave, 0);
					if((4/length)*sec>=1000){
                        note[n][2] = 999;
					} else {
					    note[n][2] = (4/length)*sec;
					}
				}
     			setFrequency(0, note[n][1]);
                setRegister(0x08, 15);
                mydelay_ms(note[n][2]);
                setRegister(0x08, 0);
                setFrequency(0, 0);
				n++;
			} else if(mml[i]=='<'){
				octave++;
			} else if(mml[i]=='>'){
				octave--;
			}
		}
	}
	return;
}

int mmldrv_main(int argc, char *argv[])
{
    configureYMZ294();
    
    while(1) {
        kz_recv(MSGBOX_ID_MMLDRIVE, NULL, NULL);
        send_write("start music.\n");
        startMML();
    }

    return 0;
}
