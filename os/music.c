#include "music.h"
#include "defines.h"
#include "kozos.h"
#include "lib.h"
#include "timerdrv.h"

static void send_start(int msec)
{
    struct timerreq *req;
    req = kz_kmalloc(sizeof(*req));
    req->id = MSGBOX_ID_MUSIC;
    req->msec = msec;
    kz_send(MSGBOX_ID_TIMDRIVE, TIMERDRV_CMD_START, (char *)req);
}

void mydelay_ms(int msec)
{
    int size;
    char *p;

    send_start(msec);
    while(1) {
        kz_recv(MSGBOX_ID_MUSIC, &size, &p);
        if(p == NULL) {
            break;
        }
        kz_kmfree(p);
    }
    return;
}

void playMusic()
{
    configureYMZ294();
    
    setFrequency(0, 440);
    setRegister(0x08, 15);
    mydelay_ms(1000);
    setRegister(0x08, 0);
    setFrequency(0, 0);
    return;
}
